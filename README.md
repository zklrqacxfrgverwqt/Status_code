# About
Get a domain's status code

## Requirements
libcurl4-openssl-dev

## How to compile:
```bash
chmod +x configure
```

```bash
./configure --prefix=/usr/bin
```

```bash
make
```

```bash
sudo make install
```
## How to use:
```bash
status_code [url]
```

### Example
```bash
status_code https://duckduckgo.com
```

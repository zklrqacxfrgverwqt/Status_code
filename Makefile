CC=gcc
CFLAGS=-Wall
LIBS=-lcurl

PREFIX=/usr/bin
SRCDIR=src

$(SRCDIR)/status_code: $(SRCDIR) status_code.c
	$(CC) -o $@ status_code.c $(LIBS)

$(SRCDIR):
	mkdir -p $(SRCDIR)

.PHONY: clean

clean:
	rm -f $(SRCDIR)/status_code

install: $(SRCDIR)/status_code
	install -Dm755 $(SRCDIR)/status_code $(PREFIX)/status_code

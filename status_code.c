#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#define USER_AGENT "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36"

size_t discardCallback(void *contents, size_t size, size_t nmemb, void *userp) {
// This function is used to discard the response body
    return size * nmemb;
}

void checkHttpStatus(const char *url) {
    CURL *curl;
    CURLcode res;

    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();

    if (curl) {
// Configure the URL
        curl_easy_setopt(curl, CURLOPT_URL, url);

// Configure not to download the response body
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);

// Configure the callback function to discard the body
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, discardCallback);

// Configure the User-Agent
        curl_easy_setopt(curl, CURLOPT_USERAGENT, USER_AGENT);

        res = curl_easy_perform(curl);

        long http_code = 0;
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

        printf("URL: %s\n", url);
        printf("HTTP Code: %ld\n", http_code);

        if (http_code >= 100 && http_code < 200) {
            printf("Information Responses\n");
        } else if (http_code >= 200 && http_code < 300) {
            printf("Successful Responses\n");
        } else if (http_code >= 300 && http_code < 400) {
            printf("Redirect messages\n");
        } else if (http_code >= 400 && http_code < 500) {
            printf("Client Error Responses\n");
        } else if (http_code >= 500 && http_code < 600) {
            printf("Server Error Responses\n");
        } else {
            printf("Unknown Response\n");
        }

        curl_easy_cleanup(curl);
    }

    curl_global_cleanup();
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "How to use:\n");
        fprintf(stderr, "    %s [url]\n", argv[0]);
        fprintf(stderr, "    %s https://duckduckgo.com\n", argv[0]);
        return EXIT_FAILURE;
    }

    checkHttpStatus(argv[1]);

    return EXIT_SUCCESS;
}
